package model;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FormImpl implements Form {
	
	private List<Athlete> listaAtletiForma = new ArrayList<>();
	public FormImpl(){
		
		this.listaAtletiForma = getListaAtletiFormaFile();
	}
		
	public String[] getScoreRed(double punti){
		
		String[] s = new String[3];
		
		if(punti < 10){
			double decimale = (punti-(int)punti)*10;	
			s[0]="res/puntitaekwondo/" + 0 + "_red.png";
			s[1]="res/puntitaekwondo/" + (int)punti + "_red.png";
			if((int)Math.ceil(decimale)==10)
				s[2]="res/puntitaekwondo/0_red.png";
			else
				s[2]="res/puntitaekwondo/" + (int)Math.ceil(decimale) + "_red.png";
			System.out.println((int)Math.ceil(decimale));
			return s;						
		}
		
		return null;		
	}
	
	public List<Athlete> getListaAtletiForma() {
	
		return listaAtletiForma;
	}
	
	public ArrayList<String> stampaAtleti(){
	
		ArrayList<String> stampata = new ArrayList<>();
		for(Athlete atleta : listaAtletiForma){
			
			stampata.add(atleta.getName()+" "+atleta.getSurname());
		}
		
		return stampata;
	}
	
	public void addAtletiForma(Athlete atleta) {
		
		this.listaAtletiForma.add(atleta);
	}
	
	public void insertListaAtletiFormaFile() {

		try {

			FileOutputStream stream = new FileOutputStream("res/storici/ListaAtletiForma.dat");
			ObjectOutputStream osStream = new ObjectOutputStream(stream);

			osStream.writeObject(listaAtletiForma);
			osStream.flush();
			osStream.close();

		} catch (Exception e) {

			System.out.println("I/O errore");
		}
	}
	
	public ArrayList<Athlete> getListaAtletiFormaFile() {

	try {

		FileInputStream stream = new FileInputStream("res/storici/ListaAtletiForma.dat");
		ObjectInputStream osStream = new ObjectInputStream(stream);

		@SuppressWarnings("unchecked")
		ArrayList<Athlete> listaAtletiFile = (ArrayList<Athlete>) osStream.readObject();

		osStream.close();
		
		return listaAtletiFile;

	} catch (Exception e) {

		System.out.println("I/O errore di stampa");
	}

	ArrayList<Athlete> arrayEmpty = new ArrayList<Athlete>();
		
		return arrayEmpty;
	}
}
