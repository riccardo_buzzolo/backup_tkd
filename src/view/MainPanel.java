package view;

import controller.MainController;

public interface MainPanel {
		
	public void addObserver(MainController controller);
}
